import React, { Fragment } from 'react';
import moment from 'moment';
import {
  getTotalTime,
  isWeekend,
  sortDays,
  showDayTime,
  showTime,
  showDiff
} from '../constants';

const weekTime = {};
let weeks = {};

export default ({ sheets, file, days }) => {
  const sortedDays = sortDays(Object.keys(days[file]));
  const lastDay = moment(sortedDays[sortedDays.length - 1], 'DD/MM/YYYY');
  const LastNextDay = lastDay.add(1, 'days');
  LastNextDay.day() === 2 && sortedDays.push(LastNextDay.format('DD/MM/YYYY'));
  return (
    <>
      <div className="list-view">
        <div className="sticky-row-1">
          <span></span>
          {sortedDays.map(day => {
            const Day = moment(day, 'DD/MM/YYYY');
            const isOddWeek = Day.isoWeek() % 2 === 0;
            return (
              <span key={day} className={isOddWeek ? 'odd-week' : ''}>
                {Day.format('ddd ll')}
              </span>
            );
          })}
        </div>
        <div className="sticky-row-2">
          <span>Heures standard</span>
          {sortedDays.map(day => {
            const Day = moment(day, 'DD/MM/YYYY');
            const isOddWeek = Day.isoWeek() % 2 === 0;
            return (
              <span key={day} className={isOddWeek ? 'odd-week' : ''}>
                {isWeekend(Day)
                  ? ''
                  : Day.day() === 5
                  ? '05h 00min'
                  : '08h 15min'}
              </span>
            );
          })}
        </div>
        {Object.entries(sheets[file]).map(([sheet, times]) => {
          weekTime[sheet] = {};
          const totalTime = getTotalTime(times);
          return (
            <Fragment key={sheet}>
              <div>
                <span>{sheet}</span>
                {sortedDays.map(day => {
                  const Day = moment(day, 'DD/MM/YYYY');
                  const time = times[Day.format('DD/MM/YYYY')] || 0;
                  const week = Day.clone()
                    .startOf('isoWeek')
                    .format('DD/MM/YYYY');
                  weekTime[sheet][week] =
                    (weekTime[sheet][week] || 0) + (isWeekend(Day) ? 0 : time);
                  const isLate =
                    Day.day() === 5
                      ? moment.duration(time).asHours() < 5
                      : moment.duration(time).asHours() < 8.25;
                  return (
                    <span
                      key={day}
                      className={
                        isWeekend(Day)
                          ? 'event-weekend'
                          : isLate
                          ? 'event-late'
                          : 'event-on-time'
                      }
                    >
                      {showDayTime(time)}
                    </span>
                  );
                })}
              </div>
              <div>
                <span
                  className={
                    moment.duration(totalTime).asHours() < 152
                      ? 'event-late'
                      : 'event-on-time'
                  }
                >
                  {showTime(totalTime)}
                </span>
                {sortedDays.map((day, index) => {
                  const Day = moment(day, 'DD/MM/YYYY');
                  const week = Day.clone()
                    .startOf('isoWeek')
                    .format('DD/MM/YYYY');
                  const time = weekTime[sheet][week];
                  const isLate = moment.duration(time).asHours() < 38;
                  if (index === 0) {
                    weeks = {};
                  }
                  if (!weeks[week]) {
                    weeks[week] = 1;
                  } else if (weeks[week] === 1) {
                    weeks[week] = 2;
                  } else {
                    weeks[week] = 3;
                  }
                  return (
                    <span
                      key={day}
                      className={
                        isWeekend(Day)
                          ? 'event-weekend'
                          : isLate
                          ? 'event-late'
                          : 'event-on-time'
                      }
                    >
                      {weeks[week] === 1
                        ? showTime(time)
                        : weeks[week] === 2
                        ? showDiff(time, 38)
                        : ''}
                    </span>
                  );
                })}
              </div>
            </Fragment>
          );
        })}
      </div>
    </>
  );
};
