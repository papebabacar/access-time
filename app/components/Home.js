import React, { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import List from './List';
import {
  getTotalTime,
  loadFile,
  saveFile,
  setDefaultSheet,
  showTime,
  showDiff
} from '../constants';

const sheets = {};
const days = {};

export default ({ path, files, setFiles, setPath, store, watcher }) => {
  const [file, setFile] = useState(store.get('file') || files[0]);
  const [sheet, setSheet] = useState();
  const [loaded, setLoaded] = useState(false);
  const [error, setError] = useState(false);
  useEffect(() => {
    if (!sheets[file]) {
      loadFile({
        file,
        path,
        setLoaded,
        setError,
        setSheet,
        sheet,
        sheets,
        days,
        toast
      });
    } else {
      setDefaultSheet({ file, setLoaded, setSheet, sheet, sheets });
    }
  }, [file]);
  return (
    <>
      <div className="toolbar">
        <span>Fichier</span>
        <select
          value={file}
          onChange={event => {
            setLoaded(false);
            setError(false);
            setFile(event.target.value);
            store.set('file', event.target.value);
          }}
        >
          {files.map(file => (
            <option key={file} value={file}>
              {file.substring(0, file.indexOf('.xlsx'))}
            </option>
          ))}
        </select>
        {loaded && !error && (
          <>
            <span>Agent</span>
            <select
              value={sheet}
              onChange={event => {
                setSheet(event.target.value);
              }}
            >
              {Object.keys(sheets[file]).map(sheet => (
                <option key={sheet} value={sheet}>
                  {sheet}
                </option>
              ))}
            </select>
            <span className="total-hours">
              {`Total : ${showTime(
                getTotalTime(sheets[file][sheet])
              )} (${showDiff(getTotalTime(sheets[file][sheet]), 152)})`}
            </span>
            <div className="right-corner">
              <a
                className="btn-extract"
                onClick={() =>
                  saveFile({
                    days: days[file],
                    sheets: sheets[file],
                    file,
                    store,
                    toast
                  })
                }
              >
                Extraire
              </a>
              <a
                className="btn-reload"
                title={path}
                onClick={() => {
                  watcher.unwatch('path');
                  setPath('');
                  setFiles([]);
                }}
              >
                Recharger
              </a>
            </div>
          </>
        )}
      </div>
      {!loaded ? (
        <div className="loading-box">
          <div className="spinner">
            <div className="bounce1"></div>
            <div className="bounce2"></div>
            <div className="bounce3"></div>
          </div>
          <div className="loading">Chargement</div>
        </div>
      ) : error ? (
        <div className="loading-box">
          Impossible de charger le fichier. Veuillez charger un autre fichier au
          bon format.
        </div>
      ) : (
        <List sheets={sheets} file={file} days={days} />
      )}
    </>
  );
};
