import { Workbook } from 'exceljs';
import { remote, shell } from 'electron';
import fs from 'fs';
import path from 'path';
import moment from 'moment';

const events = [
  'Entree Personnelle',
  'Sortie Personnelle',
  'Entrée Véhicule',
  'Sortie Véhicule'
];

const isEntrance = event => event === events[0] || event === events[2];

const isExit = event => event === events[1] || event === events[3];

export const isWeekend = Day => Day.day() === 0 || Day.day() === 6;

export const showDayTime = time =>
  time
    ? moment.utc(moment.duration(time).asMilliseconds()).format('HH[h] mm[min]')
    : '';

export const getTotalTime = times =>
  Object.entries(times).reduce((total, [day, time]) => {
    const Day = moment(day, 'DD/MM/YYYY');
    return isWeekend(Day) ? total : total + time;
  }, 0);

export const showTime = time => {
  const hours = Math.floor(time / 3600000);
  const minutes = Math.floor((time % 3600000) / 60000);
  return time
    ? (hours ? String(hours).padStart(2, '0') + 'h' : '') +
        (minutes
          ? (hours ? ' ' : '') + String(minutes).padStart(2, '0') + 'min'
          : ' 00min')
    : '';
};

export const showDiff = (time, maxHours) => {
  const duration = time - moment.duration(maxHours, 'hours').asMilliseconds();
  const sign = duration >= 0 ? '+' : '-';
  const hours = Math.floor(Math.abs(duration) / 3600000);
  const minutes = Math.floor((Math.abs(duration) % 3600000) / 60000);
  return time
    ? sign +
        (hours ? String(hours).padStart(2, '0') + 'h' : '') +
        (minutes
          ? (hours ? ' ' : '') + String(minutes).padStart(2, '0') + 'min'
          : ' 00min')
    : '';
};

export const sortDays = days => {
  days.sort(
    (day1, day2) => moment(day1, 'DD/MM/YYYY') - moment(day2, 'DD/MM/YYYY')
  );
  const dates = [days[0]];
  const currDate = moment(days[0], 'DD/MM/YYYY').startOf('day');
  const lastDate = moment(days[days.length - 1], 'DD/MM/YYYY').startOf('day');
  while (currDate.add(1, 'days').isSameOrBefore(lastDate)) {
    dates.push(currDate.clone().format('DD/MM/YYYY'));
  }
  return dates;
};

export const loadFile = ({
  file,
  path,
  setLoaded,
  setError,
  setSheet,
  sheet,
  sheets,
  days,
  toast
}) => {
  let dateColumn = 0;
  let eventColumn = 0;
  let lastEvent = '';
  let lastTime = moment('19000101', 'YYYYMMDD');
  days[file] = {};
  const setColumns = row => {
    row.eachCell((cell, cellNumber) => {
      cell.value === 'Date / heure' && (dateColumn = cellNumber);
      cell.value === 'Lecteur' && (eventColumn = cellNumber);
    });
  };
  const extract = (row, sheet) => {
    const event = row.getCell(eventColumn).value;
    const time = moment(row.getCell(dateColumn).value, 'DD/MM/YYYY HH:mm:ss');
    const day = time.format('DD/MM/YYYY');
    let sameEvent = false;
    if (day !== lastTime.format('DD/MM/YYYY')) {
      days[file][day] = days[file][day] || {};
    } else if (
      (isExit(event) && isEntrance(lastEvent)) ||
      (isEntrance(event) && isExit(lastEvent) && time.isBefore(lastTime)) ||
      (isExit(event) && isExit(lastEvent)) ||
      (isEntrance(event) && isEntrance(lastEvent) && time.isBefore(lastTime))
    ) {
      const total = sheets[file][sheet][day] || 0;
      sheets[file] = {
        ...sheets[file],
        [sheet]: {
          ...sheets[file][sheet],
          [day]: total + Math.abs(time.diff(lastTime))
        }
      };
    } else if (isEntrance(event) && isEntrance(lastEvent)) {
      sameEvent = true;
    }
    if (!sameEvent) {
      lastEvent = event;
      lastTime = time;
    }
    const lastEntrance =
      days[file][day][sheet] && days[file][day][sheet].entrance;
    const lastExit = days[file][day][sheet] && days[file][day][sheet].exit;
    days[file][day] = {
      ...days[file][day],
      [sheet]: {
        ...days[file][day][sheet],
        entrance: isEntrance(event)
          ? lastEntrance
            ? time.isBefore(lastEntrance)
              ? time
              : lastEntrance
            : time
          : lastEntrance,
        exit: isExit(event)
          ? lastExit
            ? time.isAfter(lastExit)
              ? time
              : lastExit
            : time
          : lastExit
      }
    };
    return true;
  };
  const workbook = new Workbook();
  let complete = false;
  workbook.xlsx
    .readFile(path + '/' + file)
    .then(() =>
      workbook.eachSheet(worksheet => {
        dateColumn = 0;
        eventColumn = 0;
        const sheet = worksheet.name;
        sheets[file] = { ...sheets[file], [sheet]: {} };
        worksheet.eachRow(row => {
          !(dateColumn && eventColumn)
            ? setColumns(row)
            : events.includes(row.getCell(eventColumn).value) &&
              (complete = extract(row, sheet));
        });
      })
    )
    .catch(() => {})
    .finally(() => {
      if (!complete) {
        sheets[file] = null;
        setError(true);
        setLoaded(true);
        toast(
          'Une erreur est survenue. Veuillez vérifier que le fichier est bien au bon format',
          { type: 'error' }
        );
      } else {
        setError(false);
        setDefaultSheet({ file, setLoaded, setSheet, sheet, sheets });
      }
    });
};

export const saveFile = ({ days, sheets, file, store, toast }) => {
  const { dialog, app } = remote;
  const destination = dialog.showSaveDialog({
    defaultPath: path.resolve(
      store.get('destination') || app.getPath('desktop'),
      'Access Time Export - ' + path.basename(file)
    ),
    filters: [
      {
        name: 'Feuille de calcul Excel',
        extensions: ['xlsx', 'xls', 'xlsm', 'xlsb', 'csv']
      }
    ]
  });
  destination && download({ days, sheets, destination, store, toast });
};

export const setDefaultSheet = ({
  file,
  setLoaded,
  setSheet,
  sheet,
  sheets
}) => {
  const defaultSheet = sheets[file][sheet]
    ? sheet
    : Object.keys(sheets[file])[0];
  setSheet(defaultSheet);
  setLoaded(true);
};

const download = ({ days, sheets, destination, store, toast }) => {
  const sortedDays = sortDays(Object.keys(days));
  store.set('destination', path.dirname(destination));
  const workbook = new Workbook();
  workbook.creator = 'IDEP Access Time';
  workbook.lastModifiedBy = 'IDEP Access Time';
  workbook.created = new Date();
  workbook.modified = new Date();
  workbook.addWorksheet('Access Time');
  const worksheet = workbook.getWorksheet('Access Time');
  const rows = [[]];
  let writeError = null;
  try {
    Object.entries(sheets).map(([sheet, times]) => {
      rows.push(
        ...sortedDays.reduce(
          (dayRows, day) => {
            const Day = moment(day, 'DD/MM/YYYY');
            const time = times[Day.format('DD/MM/YYYY')] || 0;
            const duration = isWeekend(Day)
              ? ''
              : Day.day() === 5
              ? '05h 00min'
              : '08h 15min';
            dayRows[0] = [...dayRows[0], Day.format('ll')];
            dayRows[1] = [...dayRows[1], Day.format('dddd')];
            dayRows[2] = [...dayRows[2], duration];
            dayRows[3] = [...dayRows[3], showDayTime(time)];
            dayRows[4] = [...dayRows[4], ''];
            dayRows[5] = [...dayRows[5], ''];
            dayRows[6] = [...dayRows[6], ''];
            return dayRows;
          },
          [
            [''],
            [''],
            ['Heures standard'],
            [sheet],
            ['Première entrée'],
            ['Dernière sortie'],
            ['Total par semaine']
          ]
        )
      );
      const totalTime = getTotalTime(times);
      rows.push(['Total', showTime(totalTime), showDiff(totalTime, 152)]);
      rows.push([]);
    });
    rows.push([]);
    worksheet.addRows(rows);
    Object.entries(sheets).map(([sheet, times], index) => {
      const argb = 'CCFBCBCB';
      const style = 'thin';
      const totalTime = getTotalTime(times);
      const totalCell = worksheet.getRow(index * 9 + 9).getCell(2);
      totalCell.font = { bold: true };
      totalCell.border = {
        top: { style, color: { argb: '#444444' } },
        left: { style, color: { argb: '#444444' } },
        bottom: { style, color: { argb: '#444444' } },
        right: { style, color: { argb: '#444444' } }
      };
      const diffCell = worksheet.getRow(index * 9 + 9).getCell(3);
      diffCell.border = {
        top: { style, color: { argb } },
        left: { style, color: { argb } },
        bottom: { style, color: { argb } },
        right: { style, color: { argb } }
      };
      diffCell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: {
          argb:
            moment.duration(totalTime).asHours() < 152 ? 'FFD41322' : 'FF167314'
        }
      };
      diffCell.font = { color: { argb: 'FFFFFFFF' } };
      const weekTime = {};
      const weekDays = {};
      worksheet.getRow(index * 9 + 5).eachCell((cell, cellNumber) => {
        if (cellNumber === 1) {
          cell.font = { bold: true };
          cell.border = {
            top: { style, color: { argb: '#444444' } },
            left: { style, color: { argb: '#444444' } },
            bottom: { style, color: { argb: '#444444' } },
            right: { style, color: { argb: '#444444' } }
          };
          return;
        }
        const day = sortedDays[cellNumber - 2];
        const Day = moment(day, 'DD/MM/YYYY');
        const time = isWeekend(Day) ? 0 : times[Day.format('DD/MM/YYYY')] || 0;
        const week = Day.clone()
          .startOf('isoWeek')
          .format('DD/MM/YYYY');
        const isLate =
          Day.day() === 5
            ? moment.duration(time).asHours() < 5
            : moment.duration(time).asHours() < 8.25;
        const background = isWeekend(Day)
          ? 'FFF8CBAD'
          : isLate
          ? 'FFD41322'
          : 'FF167314';
        const color = isWeekend(Day) ? 'FF000000' : 'FFFFFFFF';
        cell.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: background }
        };
        cell.font = { color: { argb: color } };
        !isWeekend(Day) &&
          (cell.border = {
            top: { style, color: { argb } },
            left: { style, color: { argb } },
            bottom: { style, color: { argb } },
            right: { style, color: { argb } }
          });
        weekTime[week] = (weekTime[week] || 0) + time;
        weekDays[week] = (weekDays[week] || 0) + 1;
      });
      worksheet.getRow(index * 9 + 6).eachCell((cell, cellNumber) => {
        if (cellNumber === 1) return;
        const day = sortedDays[cellNumber - 2];
        cell.value =
          days[day] &&
          days[day][sheet] &&
          days[day][sheet].entrance &&
          days[day][sheet].entrance.format('HH:mm:ss');
      });
      worksheet.getRow(index * 9 + 7).eachCell((cell, cellNumber) => {
        if (cellNumber === 1) return;
        const day = sortedDays[cellNumber - 2];
        cell.value =
          days[day] &&
          days[day][sheet] &&
          days[day][sheet].exit &&
          days[day][sheet].exit.format('HH:mm:ss');
      });
      const weekDaysComp = {};
      const weekends = {};
      worksheet.getRow(index * 9 + 8).eachCell((cell, cellNumber) => {
        if (cellNumber === 1) return;
        const day = sortedDays[cellNumber - 2];
        const Day = moment(day, 'DD/MM/YYYY');
        const week = Day.clone()
          .startOf('isoWeek')
          .format('DD/MM/YYYY');
        const time = weekTime[week];
        const isLate = moment.duration(time).asHours() < 38;
        const background = isWeekend(Day)
          ? 'FFF8CBAD'
          : isLate
          ? 'FFD41322'
          : 'FF167314';
        const color = isWeekend(Day) ? 'FF000000' : 'FFFFFFFF';
        weekDaysComp[week] = (weekDaysComp[week] || 0) + 1;
        isWeekend(Day) && (weekends[week] = (weekends[week] || 0) + 1);
        !isWeekend(Day) &&
          (weekDaysComp[week] / weekDays[week] < 0.5
            ? (cell.value = showTime(time))
            : (cell.value = showDiff(time, 38)));
        cell.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: background }
        };
        cell.font = { color: { argb: color } };
        const argb = 'CCFBCBCB';
        const style = 'thin';
        !isWeekend(Day) &&
          (cell.border = {
            top: { style, color: { argb } },
            left: { style, color: { argb } },
            bottom: { style, color: { argb } },
            right: { style, color: { argb } }
          });
        const weekendDays = weekends[week] || 0;
        const opendDays = weekDays[week] - weekendDays;
        if (
          opendDays &&
          weekDays[week] > 1 &&
          weekDaysComp[week] === weekDays[week]
        ) {
          const firstCell1 = worksheet
            .getRow(cell.row)
            .getCell(cellNumber - weekDays[week] + 1);
          const lastCell1 = worksheet
            .getRow(cell.row)
            .getCell(
              firstCell1.col +
                Math.floor(opendDays / 2) -
                (opendDays % 2 ? 0 : 1)
            );
          const firstCell2 = worksheet
            .getRow(cell.row)
            .getCell(lastCell1.col + 1);
          const lastCell2 = worksheet
            .getRow(cell.row)
            .getCell(cellNumber - weekendDays);
          if (lastCell1.master.address !== firstCell1.address) {
            worksheet.mergeCells(`${firstCell1.address}:${lastCell1.address}`);
            firstCell1.alignment = {
              vertical: 'middle',
              horizontal: 'center'
            };
          }
          if (lastCell2.master.address !== firstCell2.address) {
            worksheet.mergeCells(`${firstCell2.address}:${lastCell2.address}`);
            firstCell2.alignment = {
              vertical: 'middle',
              horizontal: 'center'
            };
          }
        }
      });
    });
    worksheet.columns.forEach((column, index) =>
      index ? (column.width = 12) : (column.width = 17)
    );
  } catch {
    writeError = true;
  }
  if (!writeError) {
    const stream = fs
      .createWriteStream(destination, { flags: 'w' })
      .on('error', error => (writeError = error));
    workbook.xlsx.write(stream).then(() => {
      if (writeError) {
        toast(
          "Une erreur est survenue. Veuillez vérifier que le fichier n'est pas déjà ouvert par une autre application",
          { type: 'error' }
        );
      } else {
        toast('Extraction réussie avec succès', { type: 'success' });
        shell.openExternal(destination, {}, () => {});
      }
    });
  } else {
    toast("Une erreur est survenue lors de l'exportation", { type: 'error' });
  }
};
