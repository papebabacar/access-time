import React, { useEffect, useState } from 'react';
import { ToastContainer } from 'react-toastify';
import moment from 'moment';
import { remote } from 'electron';
import fs from 'fs';
import chokidar from 'chokidar';
import Store from 'electron-store';
import Home from '../components/Home';

const store = new Store();
const sheets = new Set([]);
let watcher;

moment.locale('fr');

export default () => {
  const [files, setFiles] = useState([]);
  const [scanned, setScanned] = useState(false);
  const [path, setPath] = useState(store.get('path'));
  useEffect(() => {
    fs.access(
      path + '/' + store.get('file'),
      fs.F_OK,
      err => err && store.delete('file')
    );
    path
      ? (watcher = chokidar
          .watch('**/*.xlsx', { cwd: path })
          .on('add', file => {
            sheets.add(file);
            setFiles(Array.from(sheets).sort((a, b) => a < b));
          })
          .on('unlink', file => {
            file === store.get('file')
              ? store.delete('file')
              : sheets.delete(file);
            setFiles(Array.from(sheets).sort((a, b) => a < b));
          })
          .on('ready', () => setScanned(true)))
      : setScanned(true);
  }, [path]);

  return files.length ? (
    <>
      <Home
        path={path}
        files={files}
        setFiles={setFiles}
        setPath={setPath}
        store={store}
        watcher={watcher}
      />
      <ToastContainer
        autoClose={3500}
        position="bottom-right"
        style={{ fontSize: '125%' }}
      />
    </>
  ) : !scanned ? (
    <div className="loading-box">
      <div className="spinner">
        <div className="bounce1"></div>
        <div className="bounce2"></div>
        <div className="bounce3"></div>
      </div>
      <div className="loading">Chargement</div>
    </div>
  ) : (
    <div className="loading-box">
      <a
        onClick={() => {
          const { dialog } = remote;
          dialog.showOpenDialog(
            {
              properties: ['openDirectory']
            },
            newPath => {
              if (!newPath || path === newPath[0]) return;
              path && watcher && watcher.unwatch(path);
              setPath(newPath[0]);
              store.set('path', newPath[0]);
              setScanned(false);
              sheets.clear();
            }
          );
        }}
      >
        Cliquez-ici pour choisir pour selectionner un repertoire
      </a>
    </div>
  );
};
